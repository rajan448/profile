import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  exp = ( (new Date()).getTime() - (new Date('2017-10-27')).getTime()) / (1000 * 3600 * 24 * 365);

}
